module.exports = {
  schemas: {
    auths: require('./model/auths'),
    products: require('./model/products'),
    userinfo: require('./model/userinfo'),
    messages: require('./model/messages')
  }
}