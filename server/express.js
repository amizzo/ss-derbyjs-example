var express = require('express');
var stormpath = require('express-stormpath');
var expressSession = require('express-session');
var dbController = require('./dbController');
var serveStatic = require('serve-static');
var favicon = require('serve-favicon');
var compression = require('compression');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var highway = require('racer-highway');

module.exports = function (store, apps, error, publicDir, cb){

  var connectStore = require('connect-mongo')(expressSession);
  var sessionStore = new connectStore({url: process.env.MONGO_URL});

  var session = expressSession({
    secret: process.env.SESSION_SECRET,
    store: sessionStore,
    cookie: process.env.SESSION_COOKIE,
    saveUninitialized: true,
    resave: true
  });

  var handlers = highway(store, {session: session});

  var expressApp = express()
    .use(favicon(publicDir + '/img/favicon.ico'))
    .use(express.static(__dirname + '/public/assets'))
    .use(compression())
    .use(serveStatic(publicDir))
    .use(store.modelMiddleware())
    .use(cookieParser())
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({extended: true}))
    .use(session)
    .use(handlers.middleware)
    .use(createUserId)
    // .use(setPageSession)
    .use(useStormpath);

  apps.forEach(function(app){
    expressApp.use(app.router());

      expressApp.use(stormpath.init(expressApp, {
          apiKeyId:     process.env.STORMPATH_API_KEY_ID,
          apiKeySecret: process.env.STORMPATH_API_KEY_SECRET,
          secretKey:    process.env.STORMPATH_SECRET_KEY,
          application:  process.env.STORMPATH_URL,
          expandGroupMemberships: true,
          cache: 'redis',
          cacheHost: '127.0.0.1',
          cachePort: 6379
      }));

  });

  expressApp.use(require('./routes'));

  expressApp
    .all('*', function (req, res, next) { next('404: ' + req.url); })
    .use(error);

  cb(expressApp, handlers.upgrade);
}

function createUserId(req, res, next) {
  var model = req.getModel();
  var userId = req.session.userId;
  if (!userId) userId = req.session.userId = model.id();
  model.set('_session.userId', userId);
  next();
}

function useStormpath (req, res, next) {
    var model = req.getModel();
    var stormUser = req.session.stormUser;
    if(stormUser) {
        model.set('_session.stormUser', stormUser);
    }
    next();
}

// module.exports.getModel = getModel;