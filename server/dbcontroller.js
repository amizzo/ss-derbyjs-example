/**
 * Created by Anthony on 1/22/15.
 */

var async = require('async');
var mysql = require('mysql');
var needle = require('needle');
var uuid = require('node-uuid');

// var model = require('./express').getModel;

dbSql = function() {};
dbCb = function() {};
dbWp = function() {};
db4d = function() {};

dbSql.getUserId = function(model, product, inputQuery, callback) {

    switch (product) {
        case 'OHS Insider':
            var connection = mysql.createConnection(
                {
                    host: '--host--',
                    user: '--user--',
                    password: '--password--',
                    database: '--db--'
                }
            );
            break;
        case 'SafetySmart Compliance':
            var connection = mysql.createConnection(
                {
                    host: '--host--',
                    user: '--user--',
                    password: '--password--',
                    database: '--db--'
                }
            );
            break;
    }

    connection.connect();
    var thisQuery = connection.query(inputQuery);
    thisQuery.on('error', function(err) {
        model.add('messages', { time: new Date(), type: 'error', content : err});

        // model.toast('error', err);
        callback(err);

    });
    thisQuery.on('result', function(row) {
        callback(row.ID);
    });
    connection.end();
};
dbWp.logTest = function(model, thisMsg) {
    // model.set('_page.log', 'Test at thismsg');
    // model.toast('success', thismsg);
    console.log(thisMsg);
    // console.log('Model _session._page is: ' + JSON.stringify(model.get('_session._page')));
    model.add('messages', { time: new Date(), type: 'info', content : thisMsg});
    // model.set('messages.'+uuid.v4()+'.info', 'Yolo');
    // console.log(JSON.stringify(model.get('messages')));

}

dbWp.logTestModel = function(model) {
    // model.set('_page.log', 'Test at thismsg');
    // model.toast('success', thismsg);
    console.log('Model _session is: ' + JSON.stringify(model.get('_session')));
    model.set(String('wpUserId.' + uuid.v4() + '.user_id'), uuid.v1());


};

dbWp.createUser = function(model, thisPost) {
    var thisUuid = uuid.v1();
    //model.set('userinfo.user_id', thisUuid);

    console.log('Received starting command for createUser()');
    model.add('messages', {content: 'Starting to create user...'});
    var thisModelId = model.id();
    console.log('Model ID: '+ thisModelId);
    switch (thisPost.thisProduct) {
        case 'OHS Insider':
            var thisUrl = '--url-1--';
            break;
        case 'SafetySmart Compliance':
            var thisUrl = '--url-2--';
            break;
    }
    async.waterfall([

        // Get nonce auth from wordpress
        function(callback) {
            //model.add('messages', {content: 'Starting async requests..'});
            needle.get(thisUrl + '/api/get_nonce/?controller=user&method=register', function(error, response) {
                if (!error && response.statusCode == 200) {
                    console.log(response.body);
                    callback(null, response.body.nonce);
                   model.add('messages', {content: 'Authenticating with wordpress...'});
                }
                if(error) {
                    model.add('messages', {content: 'Failed with error: ' + JSON.stringify(error)});
                    callback(error, null);
                }
            });
        },
        // Construct user reg URL using nonce from previous function
        function(thisNonce, callback) {
            console.log("This nonce is: " + thisNonce);
            console.log(thisPost);
            //model.push(String('messages.'+thisUuid+'.content'), ['Registering User...']);
            needle.post(thisUrl + '/api/user/register', 'username=' + thisPost.username + '&display_name=' + thisPost.firstName + '%20' + thisPost.lastName + '&email=' + thisPost.email + '&first_name=' + thisPost.firstName + '&last_name=' + thisPost.lastName + '&user_pass=' + thisPost.userPass + '&role=' + thisPost.thisRole + '&nonce=' + thisNonce, function(error, response) {
                if (!error && response.statusCode == 200 && response.body.status != "error") {
                    var thisResp = response.body;
                    var delResp = thisResp.replace("cannot be blank", "");
                    var propResp = JSON.parse(delResp);
                    console.log(propResp);
                    console.log(propResp.user_id);
                   // model.set('userinfo.user_id', propResp.user_id );
                    model.set(String('wpUserId.' + uuid.v4() + '.user_id'), propResp.user_id);


                    callback(null, propResp);
                   // model.push(String('messages.'+thisUuid+'.content'), ['User registered successfully']);
                }
                if (error || response.body.status == "error") {
                    //model.push(String('messages.'+thisUuid+'.content'), 'Failed with error: ' + JSON.stringify(response.body.error));
                    callback(response.body.error, null);
                }

            });
        },
        // Get the user ID of the user we just created
        function(respBody, callback) {
            //model.push(String('messages.'+thisUuid+'.content'), 'Adding user id for Chargebee...');
            callback();
            /*
            this.dbSql.getUserId(model, thisPost.thisProduct, "SELECT ID from wp_users WHERE user_email LIKE '%" + thisPost.email + "%'", function(response) {
                callback(null, response);
                model.add('messages', { time: new Date(), type: 'info', content : 'Response give: ' + response});
                if (response) {
                   // model.set('userinfo.chargebee.wpUserId', response);
                    console.log(response);
                }
            });
            */

        }

    ], function(err, result) { //This function gets called after the two tasks have called their "task callbacks"
        if (err) {
            //model.push(String('messages.'+thisUuid+'.content'), JSON.stringify(err));
        } else if (result) {
           // model.push(String('messages.'+thisUuid+'.content'), JSON.stringify(result));

        }
    });

};

dbCb.createCustomer = function(model) {
    switch (model.get('_page.thisCurrency')) {
        case 'CAD':
            var thisSite = "safetysmart-cad";
            chargebee.configure({site : "safetysmart-cad",
                api_key : "--apikey--"});
            break;
        case 'USD':
            var thisSite = "safetysmart";
            chargebee.configure({site : "safetysmart",
                api_key : "api--key"});
            break;
    }

    chargebee.customer.create({
        id: model.get('userinfo.user_id'),
        first_name : model.get('_page.firstName'),
        last_name : model.get('_page.lastName'),
        email : model.get('_page.email'),
        company : model.get('_page.company'),
        phone : model.get('_page.phone')
    }).request(function(error,result){
        if(error){
            //handle error
            console.log(error);
            model.toast('error', error);
            return false;
        }else{
            console.log(result);
            model.toast('success', result);
            var customer = result.customer;
            model.set('_page.respUrl', "https://" + thisSite + ".chargebee.com/admin-console/customers/" + result.customer.id);
            return true;
        }
    });
};



exports.dbSql = dbSql;
exports.dbCb = dbCb;
exports.dbWp = dbWp;
exports.db4d = db4d;