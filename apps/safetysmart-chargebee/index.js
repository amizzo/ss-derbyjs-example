var derby = require('derby');
// var stormpath = require('express-stormpath');
var app = module.exports = derby.createApp('safetysmart-chargebee', __filename);
var path = require('path');

// if (!derby.util.isProduction) global.app = app;


app.use(require('d-bootstrap'));
app.use(require('derby-router'));
app.use(require('derby-debug'));

app.serverUse(module,'derby-jade');
app.serverUse(module,'derby-stylus');
app.loadViews(__dirname + '/views');
app.loadStyles(__dirname + '/styles');
app.component(require('derby-ui-toast'));

require('./createuser');

app.get('home', '/');
