var derby = require('derby');
// var stormpath = require('express-stormpath');
var path = require('path');
var app = require('./index');
var uuid = require('node-uuid');
// if (!derby.util.isProduction) global.app = app;

app.proto.postLog = function() {
    this.model.set('_page.log', JSON.stringify(this.model.get('_page')));
    // this.model.set('_page.log', dbWp.logTest('Test From createuser.js'));
};

app.proto.submitWp = function() {
    var thisObj = {
        id: uuid.v1()
    };

    for(var key in this.model.get('_page')) {
        thisObj[key] = this.model.get('_page')[key];
    }

    var options =
    {
        type: "POST",
        url: '/api/v1/item',
        data: thisObj,
        success: null,
        dataType: JSON
    };


    return $.ajax(options);

    /*
    this.model.set('_session.page', '_page');
    var url = '/api/v1/item';
    return httpGet(url, function(res) {
        if (res.status !== 200) {
        // update UI with error msg?
        } else {
        // update UI with success msg!
        }
    });
    //Dbcontroller.createWpUser(this.model);
    */

};
app.proto.logTest = function() {
    dbWp.logTest(this.model, 'Hello');
}
app.proto.submitCb = function() {
    console.log(this.model.get('_page'));
    //Dbcontroller.createCbCustomer(this.model);

};
app.proto.clearPage = function() {
    for(var key in this.model.get('_page')) {
        this.model.set(String('_page.'+key), null);
    }
};
app.serverGet('test', '/api/test', function(req, res, next) {
    this.model.del('wpUserId.user_Id', this.model.set('wpUserId.user_id', uuid.v4(), res.json(this.model.get('wpUserId.user_id'))));
    //res.json(this.model.get('wpUserId.user_id'));
});

app.serverGet('del', '/api/del', function(req, res, next) {
    this.model.destroy('wpResp');
    res.json(this.model.get('wpResp'));
});

app.serverGet('get', '/api/get', function(req, res, next) {

    this.model.set(String('wpUserId.' + uuid.v4() + '.user_id'), uuid.v1());
    res.json(this.model.get('wpUserId'));
    //this.model.set('userinfo.user_id', 'serverGet');

});

app.get('createuser', '/createuser', function(page, model) {
    model.subscribe('messages', 'wpUserId', function() {

        var thisStormUser = model.get('_session.stormUser');

        model.on('all', 'wpUserId**', function(event, index, thisMsg){
            model.set('_page.wpUserId', thisMsg.user_id);
        });
        model.on('all', 'mesasages**', function(event, index, thisMsg){
            //model.set('_page.messages', thisMsg);
            console.log(thisMsg);
        });
      //  if (thisStormUser) {
            page.render('createuser');
       // } else {
           // page.redirect('/login');
        //}

    });
    /*
    model.on('set', 'messages.*', function (value, previous, passed) {
        // console.log('Index: ' + index + '; Msg: ' + JSON.stringify(thisMsg));
        console.log(JSON.stringify(passed));
        model.toast('info', passed);

    });
*/

    /*
    model.subscribe('userinfo', function() {
        model.on('load', 'userinfo.user_id', function(index, thisMsg) {
            console.log(thisMsg);
            model.set('userinfo.user_id', thisMsg);
        });
        model.on('unload', 'userinfo.user_id', function(index, thisMsg) {
            console.log(thisMsg);
            model.setNull('userinfo.user_id');
        });
    });
    */

});
app.serverGet('time', '/api/time', function(req, res, next){
    res.json(Date.now());

});
app.serverPost('userid', '/api/wpUserId', function(req, res, next){
    res.json(Date.now());
    res.json(req.body);
    this.model.set('userinfo.user_id', '1234');

});

module.exports = app;

/**
 * Created by Anthony on 1/26/15.

var express = require('express');
var router = express.Router();
var app = require('./index');
var stormpath = require('express-stormpath');
var async = require('async');
var dbWp = require('./dbcontroller.js').dbWp;

router.get('/', stormpath.groupsRequired(['admins']), function(req, res) {

    res.render('createuser', { title: 'SafetySmart - Chargebee', path: 'createuser', user: req.user });
});

router.post('/newInWp', function(req,res) {
    if(!req.body) {
        res.statusCode = 400;
    }
    res.json(true);
    console.log(req.body);
    if (!req.body.chkTest && req.body.username && req.body.userEmail && req.body.userPassword){
        dbWp.createUser(req.body.userProduct, req.body.username, req.body.userEmail, req.body.userFirstName, req.body.userLastName, req.body.userPassword, req.body.userRole, req.body.userSubStart, req.body.userSubEnd);
    }
});

router.post('/newInCb', function(req,res) {
    if(!req.body) {
        res.statusCode = 400;
    }
    res.json(true);
    console.log(req.body);
    www.io.emit('log', JSON.stringify(req.body));


});

router.post('/textChanged/:thisSrcElem/:thisTargetElem', function(req,res) {
    if(!req.body) {
        res.statusCode = 400;
    }
    var thisSrcElem = req.params.thisSrcElem;
    var thisTargetElem = req.params.thisSrcElem.thisTargetElem;

    www.io.emit('log', JSON.stringify(req.body));
    res.send("<input class='form-control' type='text' name='" + thisTargetElem + "' id='#" + thisTargetElem + "' value='" + req.body[req.params.thisSrcElem] + "' />");
});

module.exports = router;
 */